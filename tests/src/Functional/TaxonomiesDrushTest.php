<?php

namespace Drupal\Tests\term_trans_ex_im\Functional;

use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\term_trans_ex_im\Traits\TaxonomiesSetUpTrait;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests basic import and export functionalities using drush.
 *
 * @group term_trans_ex_im
 */
class TaxonomiesDrushTest extends BrowserTestBase {

  use DrushTestTrait;
  use TaxonomiesSetUpTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'term_trans_ex_im',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The taxonomy term storage handler.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taxonomyTermStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createSetUpContent();
  }

  /**
   * Tests exporting and importing of term items entities using drush command.
   */
  public function testTaxonomiesExportImportUsingDrush() {
    // Exporting terms.
    $this->drush('ttei-exp fr');
    $taxonomies = $this->config('term_trans_ex_im.data')->get('taxonomies');
    $this->assertCount(3, $taxonomies);

    // Editing the "Oxygen" taxonomy term.
    $oxygenTerms = $this->taxonomyTermStorage->loadByProperties(['name' => 'Oxygen']);
    reset($oxygenTerms)->set('description', 'Oxygen')->save();

    // Deleting the "Tig gas" taxonomy term.
    $tigGasTaxonomiesLinkBefore = $this->taxonomyTermStorage->loadByProperties(['name' => 'Argon']);
    $this->taxonomyTermStorage->delete($tigGasTaxonomiesLinkBefore);
    $this->assertEmpty($this->taxonomyTermStorage->loadByProperties(['name' => 'Argon']));

    // Adding an "Electrode" taxonomy term.
    Term::create([
      'vid' => 'arc',
      'name' => 'Electrode',
    ])->save();

    // Importing terms using safe mode.
    $this->drush('ttei-imp ../taxonomy-terms-ouput__fr.csv');

    // Deleted term should be imported.
    $tigGasTerms = $this->taxonomyTermStorage->loadByProperties(['name' => 'Argon']);
    $this->assertNotEmpty($tigGasTerms);

    // "Oxygen" taxonomy term should not be updated by a safe import.
    $oxygenTerms = $this->taxonomyTermStorage->loadByProperties(['name' => 'Oxygen']);
    $this->assertEquals('Oxygen', reset($oxygenTerms)->description->value);

    // "Electrode" taxonomy term should not be deleted by a safe or full import.
    $electrodeTerms = $this->taxonomyTermStorage->loadByProperties(['name' => 'Electrode']);
    $this->assertNotEmpty($electrodeTerms);

    // Importing terms using full mode.
    $this->drush('ttei-imp ../taxonomy-terms-ouput__fr.csv');

    // "Electrode" taxonomy term should be deleted by full import.
    $electrodeTerms = $this->taxonomyTermStorage->loadByProperties(['name' => 'Electrode']);
    $this->assertEmpty($electrodeTerms);

    // Importing terms using force mode.
    $this->drush('ttei-imp ../taxonomy-terms-ouput__fr.csv');

    // "Oxygen" taxonomy term should be updated by a full import.
    $oxygenTerms = $this->taxonomyTermStorage->loadByProperties(['name' => 'Oxygen']);
    $this->assertEquals('Oxygen gas', reset($oxygenTerms)->description->value);
  }

}
